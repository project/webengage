WebEngage 7.x-1.0.0, 2014-03-13
-------------------------------
Version 2.0.2. Supporting Drupal 7.x.
Security fixes.
Reduced dependency on iFrames.
CSS changes.

WebEngage 7.x-1.0.0, 2013-07-31
-------------------------------
Initial release of webengage module. Supporting Drupal 7.x.
