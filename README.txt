
-- SUMMARY --

This is an offical WebEngage module which lets you embed the Javascript 
integration code in to your pages without editing templates. 
Install the module once. No need to change or write a single line of 
code to make it work. Ever!

With WebEngage you can do the following:
----------------------------------------
  1. Get feedback from your customers. 
	We make it easy for them to report issues or suggest ideas. 
	And we make it easier for you by offering a nice management console 
	to reply back, keep a track and anlayze all your data.
      
  2. Collect customer insights by conducting in-site short surveys. 
	You can run product feedback survey, customer satisfaction surveys, 
	lead generation surveys etc. Analyze all the data using our powerful 
	reporting and analytics modules.
	
  3. Drive conversion and push sales on your website using our 
     in-site notifications. 
	You can use notifications to offer discount code, push a feature 
	update message to all your visitors or announce a downtime on your 
	website. You can target these messages at specific audiences on 
	your website to create an effective campaign. We offer powerful 
	analytics for each of your notifications including statistics like 
	clickthrough rates, country wise distribution etc.

Features
--------
    1. Places a customizable "Feedback" tab to your site - 
	customize colors and placement.
    2. Receive unlimited feedback. Automatic screengrab of 
	the page that a user submits the feedback on.
    3. Recieve email notifications. Reply to feedback via 
	email or from your WebEngage dashboard.
    4. Mark feedback threads as resolved, open or unread.
    5. Create short surveys for all or particular pages on your site.
    6. Target these surveys on multiple parameters - visitor geography, 
	site referrals, cookies, first-time-visitors etc.
    7. Collect responses for these surveys, view and download reports. 
	See demographic distribution.
    8. Display push notifications on your website.
    9. Target these notification messages at certain audience segments 
	on your website.
    10. Get detailed stats on the performance of these push notifications 
	including data like clickthrough rate etc.
    11. Works in all browsers (including IE6!).
    12. Non-blocking, high-performance code.

Details and product screenshots
-------------------------------
    Feedback - webengage.com/feedback
    Survey - webengage.com/survey
    Notification - webengage.com/notification

Localization
------------
    WebEngage is currently available in 30 languages - English, Arabic, 
    Bulgarian, Chinese (Traditional), Czech, Danish, Dutch, Estonian, Finnish, 
    French, Georgian, German, Greek, Hebrew, Hindi, Hungarian, Italian, 
    Japanese, Korean, Norwegian, Polish, Portuguese, Romanian, Russian, 
    Slovak, Slovenian, Spanish, Swedish, Thai and Turkish. If your language 
    is missing, you can help us translate. There are just 30 odd phrases 
    that need to be translated. Let us know if you are interested.

Demo
----
    Take an online demo to get an idea of how the feedback tab, survey 
    windows and notification messages will look on your website - 
      demo.webengage.com 
	
For a full description of the module, visit the project page:
-------------------------------------------------------------
    http://drupal.org/project/webengage

To submit bug reports and/or feature suggestions, drop a mail at
----------------------------------------------------------------
    support@webengage.com


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Once the install is completed, you need to enable the 
    'WebEngage Script Block'.
    Go to Admin -> Structure -> Blocks -> Search for 'WebEngage' -> and 
    choose appropriate region(prominently the 'Footer' one, if available 
    in this theme)

* Also, in case you have changed the theme, and module doesn't show up 
    on client-side, it probably is due to the new theme NOT supporting
    the 'region' to which WebEngage block was added. To resolve this, simple 
    change the region to sidebar, or content, if available
    
* 'WebEngage Settings' page is available at:
    admin/webengage/action/main
    You can also click on WebEngage menu item to reach the same: Admin 
    -> Structure -> Menus -> WebEngage Settings -> list links 
    -> 'WebEngage Settings'


    
-- CONFIGURATION --

* Visit WebEngage Settings page and wait for the page to load. 
    Then sign-up for WebEngage account 
    and hence forth follow the instructions detailed under 
    'INSTALLATION' heading

    
-- TROUBLESHOOTING --

* If the module isn't working as desired, contact us at
    support@webengage.com


-- FAQ --

Q: Is WebEngage FREE to use? 

A: - Yes. A feedback form with auto-screengrab feature, unlimited feedback 
      queries and responses every month, short survey with limited number 
      of responses and targeting capabilities - all these are FREE forever. 
      Of-course, if you need more, we have some other cool features for 
      which you can upgrade to our premium plans.


Q: The feedback tab shows up on my website. However, upon clicking, it 
      displays an error message instead of the feedback form. Why? 

A: - Most likely because the domain name for which you created the widget 
      does not match with the one you are using it on. If you are keen to 
      test WebEngage in test/dev environment, please create a separate 
      widget with "localhost" or a corresponding domain name. Use the 
      corresponding license code in your plugin to test locally.


Q: What all information does WebEngage capture? 

A: - Apart from the answers to questions in your forms (both feedback 
      and survey) and a screengrab of the page on which the feedback was 
      submitted on, we automatically capture a whole bunch of other 
      information. E.g. Geo location (and IP) of the user submitting 
      feedback, Date/time, Page URL on which the feedback was submitted, 
      Browser, Operating Platform etc. You get to see all of these in the 
      email notification as well as in your WebEngage dashboard.


Q: Can I add custom fields to the feedback form? 

A: - Yes. With paid plans (starting $15/month), you can add your own fields 
      to the feedback form - checkboxes, radio buttons, input boxes, comment 
      boxes, files, dropdowns etc. This can be done using a simple 
      drag-n-drop form builder inside WebEngage dashboard. You don't need to 
      write any code whatsoever.


Q: Is WebEngage spam-free? 

A: - Yes. Our technology makes sure that only humans can submit the feedback 
      form on your website. And we do that without asking your users to read 
      through an unreadable CAPTCHA image :). Our stuff just works!


Q: Can data be exported from WebEngage? 

A: - Yes. You can download all your feedback in Excel format in realtime. 
      Responses to all your surveys can be downloaded in similar fashion. 
      Very soon, we'll release our API which will make it easier to push 
      all the WebEngage data into your favourite CRM.


Q: What other customizations are possible? 

A: - Far too many! All email templates, stylesheets, logos etc can all be 
      changed depending upon the plan you choose.


  For any other question(s), please email us on 
      support@webengage.com or contact us. We assure you of a quick revert.


-- CONTACT --

    Website: 		http://webengage.com
    FAQ's: 		http://webengage.com/faq
    Support mail:	support@webengage.com
    Pricing:		http://webengage.com/pricing
    Contact No.:	+1 (408) 890-2392 (US)
			+91 (22) 67259495 (IN)
