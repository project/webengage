<?php
/**
 * @file
 * constants file
 *
 * @category constants
 * @package   WebEngage
 * @link     http://www.webengage.com/
 */

  define("PATH_MAIN", "admin/webengage/action/main");
  define("PATH_CALLBACK", "admin/webengage/action/callback");
